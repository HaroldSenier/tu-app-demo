﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TuMvcApp.Services;

namespace TuMvcApp.Controllers
{
	public class CategoriesController : Controller
	{
		private readonly IHttpClientFactory _httpClientFactory;
		private readonly IHostingEnvironment _he;
		private readonly string apiEndPoint;
		private ApiEndPointSettings _appSettings { get; set; }

		public CategoriesController(IHttpClientFactory httpClientFactory, IHostingEnvironment he, IOptions<ApiEndPointSettings> appSettings)
		{
			_httpClientFactory = httpClientFactory;
			_he = he;
			_appSettings = appSettings.Value;

			if (_he.IsDevelopment())
				apiEndPoint = _appSettings.Dev;
			else
				apiEndPoint = _appSettings.Cloud;
		}
		// GET: Categories
		public ActionResult Index()
		{
			return View();
		}

		// GET: Categories/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: Categories/Create
		public ActionResult Create()
		{
			return View();
		}

		// GET: Categories/Edit/1
		public async Task<IActionResult> Edit(int id)
		{

			var client = _httpClientFactory.CreateClient();
			client.BaseAddress = new Uri(apiEndPoint);

			var result = await client.GetStringAsync("/api/Categories/Get?id=" + id);

			return PartialView("~/Views/Categories/_edit.cshtml", JsonConvert.DeserializeObject(result));
		}

		//// POST: Categories/Create
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Create(IFormCollection collection)
		//{
		//    try
		//    {
		//        // TODO: Add insert logic here

		//        return RedirectToAction(nameof(Index));
		//    }
		//    catch
		//    {
		//        return View();
		//    }
		//}

		//// GET: Categories/Edit/5
		//public ActionResult Edit(int id)
		//{
		//    return View();
		//}

		//// POST: Categories/Edit/5
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit(int id, IFormCollection collection)
		//{
		//    try
		//    {
		//        // TODO: Add update logic here

		//        return RedirectToAction(nameof(Index));
		//    }
		//    catch
		//    {
		//        return View();
		//    }
		//}

		//// GET: Categories/Delete/5
		//public ActionResult Delete(int id)
		//{
		//    return View();
		//}

		//// POST: Categories/Delete/5
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Delete(int id, IFormCollection collection)
		//{
		//    try
		//    {
		//        // TODO: Add delete logic here

		//        return RedirectToAction(nameof(Index));
		//    }
		//    catch
		//    {
		//        return View();
		//    }
		//}
	}
}