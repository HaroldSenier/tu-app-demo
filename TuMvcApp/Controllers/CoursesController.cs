﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TuMvcApp.Services;

namespace TuMvcApp.Controllers
{
	public class CoursesController : Controller
	{
		private readonly IHttpClientFactory _httpClientFactory;
		private readonly IHostingEnvironment _he;
		private readonly string apiEndPoint;
		private ApiEndPointSettings _apiEndPointSettings { get; set; }

		public CoursesController(IHttpClientFactory httpClientFactory, IHostingEnvironment he, IOptions<ApiEndPointSettings> apiEndPointSettings)
		{
			_httpClientFactory = httpClientFactory;
			_he = he;
			_apiEndPointSettings = apiEndPointSettings.Value;

			if (_he.IsDevelopment())
				apiEndPoint = _apiEndPointSettings.Dev;
			else
				apiEndPoint = _apiEndPointSettings.Cloud;
		}
		public IActionResult Index()
		{
			return View();
		}


		// GET: Courses/Create
		public IActionResult Create()
		{
			return View();
		}

		// GET: Courses/Catalog
		public IActionResult Catalog()
		{
			return View();
		}

		// GET: Courses/Search
		public IActionResult Search(string q)
		{
			return View();
		}

		// GET: Courses/List
		public IActionResult List()
		{
			return View();
		}

		// GET: Courses/Edit/{id}
		public async Task<IActionResult> Edit(int id)
		{
			var client = _httpClientFactory.CreateClient();
			client.BaseAddress = new Uri(apiEndPoint);

			var result = await client.GetStringAsync("/api/Courses/Get/" + id);


			return View(JsonConvert.DeserializeObject(result));
		}
	}
}