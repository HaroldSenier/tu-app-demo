﻿//global variables

//global functions
var myLazyLoad; 
function lazyLoadImages() {
    myLazyLoad = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300 //adjust according to use case
    });
}

//function updateLazyLoadImages() {
//    myLazyLoad.update();
//}

function courseCardMaker(course) {
    return `<div class="col-md-4">
                <div class="card mb-4 addborder-on-hover animated fadeIn">
                    <img class="card-img-top card-image lazy" src="/images/loader.gif" data-src="${course.courseImage}"  onerror="this.onerror=null;this.src='https://www.gaudi-clothing.com/assets/images/image-not-found.svg';" alt="Card image cap" width="290px" height="250px">
                    <div class="card-body">
                        <h5 class="card-title">${course.title}</h5>
                        <p class="card-text">${course.description}</p>
                        <a href="/" class="btn btn-dark btn-sm">Go somewhere</a>
                    </div>
                </div>
            </div>`;
}

//Search
function loadSearchResult() {

    var urlParams = new URLSearchParams(window.location.search);
    var searchString = urlParams.get('q');
    apiSearchCourse = apiEndpoint + `/Courses/Search?q=${searchString}`;
    $("#txtSearch").val(searchString);
    $.get(apiSearchCourse)
        .done(function (courses) {
            $('#courseCatalogContainer').empty();
            let ms = 200;
            let i = 1;
            if (courses.length > 0) {
                $.each(courses, function (i, course) {
                    var item = courseCardMaker(course);
                    setTimeout(function () {
                        $('#courseCatalogContainer').append(item);
                        lazyLoadImages();
                    }, ms * i);
                });
            } else {
                var item = `No result found.`;
                $('#courseCatalogContainer').append(item);
            }


        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            swal({
                title: "Error",
                text: jqXHR.statusText,
                icon: "error",
                button: "Close",
            });
        })

}

function searchCourse(searchString) {
    window.location.href = "/Courses/Search?q=" + searchString;
}



