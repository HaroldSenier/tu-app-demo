﻿var apiGetCourses = apiEndpoint + "/Courses"
var coursesList;
var coursesTable;

function initializeDatePickers() {
    var dps = ["dpEnddate", "dpStartdate"]
    dps.map(function (dp) {
        $("#" + dp).datetimepicker({
            format: 'L',
            useCurrent: false
        });
    });
}

function CreateCourse() {

    let Title = $("#txtTitle").val().trim();
    let CategoryId = $("#ddSubcategory").val();
    let Description = $("#txtDescription").val().trim();
    let AccessMode = $("#ddAccessMode").val().trim();
    let StartDate = $("#txtStartdate").val().trim();
    let EndDate = $("#txtEnddate").val().trim();
    let PublishedBy = $("#txtPublishedBy").val().trim();
    let UpdatedBy = "10146063";
    let Duration = $("#txtDuration").val().trim();
    let CourseImage = $("#txtCourseImage").val().trim();
    let CourseTypeId = $("#ddCourseType").val().trim();

    //TEsting
    //Title = "Course test 1";
    //CategoryId = 1;
    //Description = "lorem";
    //AccessMode = 1;
    //StartDate = "";
    //EndDate = "";
    //PublishedBy = "";
    //UpdatedBy = "";
    //Duration = "";
    //CourseImage = "";
    //CourseTypeId = "";
    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Courses/Create",
        crossDomain: true,
        data: JSON.stringify({
            "Title": Title,
            "CategoryId": CategoryId,
            "Description": Description,
            "AccessMode": AccessMode,
            "StartDate": StartDate,
            "EndDate": EndDate,
            "PublishedBy": PublishedBy,
            "UpdatedBy": UpdatedBy,
            "Duration": Duration,
            "CourseImage": CourseImage,
            "CourseTypeId": CourseTypeId
        }),
        success: function (data, textStatus, jqXHR) {
            var categoryName = data.name;
            console.log(data);
            console.log(jqXHR);
            swal({
                title: "Success",
                text: jqXHR.statusText,
                icon: "success",
                button: "Close",
            }).then((response) => {
                window.location = "/Courses/Catalog";
            });
            //alert(jqXHR.statusText + " " + categoryName);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                $("#validationSummary").empty();
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.statusText,
                    icon: "error",
                    button: "Ok",
                });

            }
        }
    });
}

function LoadAllCourses() {
    $.get(apiGetCourses)
        .done(function (courses) {
            coursesList = courses;
            $('#courseCatalogContainer').empty();
            if (courses.length > 0) {

                let ms = 200;
                let i = 1;
                $.each(courses, function (i, course) {
                    var item = courseCardMaker(course);
                    setTimeout(function () {
                        $('#courseCatalogContainer').append(item);
                        lazyLoadImages();
                    }, ms * i);

                });
            } else {
                $('#courseCatalogContainer').html('No course to display.');
            }

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            swal({
                title: "Error",
                text: jqXHR.statusText,
                icon: "error",
                button: "Close",
            });
        })

}

function LoadCoursesIntoTable(tableName) {
    coursesTable = tableName;

    $('#' + coursesTable).DataTable({
        //"processing": true,
        "ajax": {
            "url": apiGetCourses,
            "method": "GET",
            "dataSrc": function (data) {
                coursesList = data;
                return data;
            },
        },
        "columns": [
            { "data": "id" },
            { "data": "courseType" },
            { "data": "title" },
            { "data": "description" },
            { "data": "category.parent.name" },
            { "data": "category.name" },
            {
                "data": "id", "render": function (data, type, row, meta) {
                    return `<a href="#" class="editor_edit" data-id=${data}>Edit</a> / <a href="#" class="editor_remove" data-id=${data}>Delete</a>`
                }
            }
        ],
        "language": {
            "sSearch": "Filter results:",
            //'processing': 'Loading...'

        }
    });

    // Edit record
    $('#' + coursesTable).on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        let id = $(this).attr("data-id");
        window.location.href = "/Courses/Edit/" + id;
    });

    // Delete a record
    $('#' + coursesTable).on('click', 'a.editor_remove', function (e) {
        e.preventDefault();

        let id = $(this).attr("data-id");
        var course = coursesList.find(c => c.id == id);

        const courseId = course.id;
        const courseName = course.title;

        swal({
            title: "Confirm",
            text: `Are you sure you want to delete ${courseName}?`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostDeleteCourse(courseId);
                }
            });

    });

}

function ReloadCourseTable() {
    $("#" + coursesTable).DataTable().ajax.reload(function (data) {
        coursesList = data;
    });
}

function loadEditForm(editCourse) {
    //set form values
    //$("#_editCourseModal").modal("toggle");
    //set form course lbl

    debugger;
    //get course detail from retrieved value
    //var editCourse = coursesList.find(c => c.id == id);

    let Title = editCourse.title;
    let CategoryId = editCourse.category.parentId;
    let SubcategoryId = editCourse.categoryId;
    let Description = editCourse.description;
    let AccessMode = editCourse.accessMode;
    let StartDate = editCourse.startDate;
    let EndDate = editCourse.endDate;
    let PublishedBy = editCourse.publishedBy;
    let UpdatedBy = "10146063";
    let Duration = editCourse.duration;
    let CourseImage = editCourse.courseImage;
    let CourseTypeId = editCourse.courseTypeId;


    initializeDatePickers();
    $("#lblCourseName").html(Title);
    //load form values
    $("#txtId").val(id);
    $("#ddCourseType").val(CourseTypeId);
    $("#txtTitle").val(editCourse.title);
    loadEditFormCategories(CategoryId);
    loadEditFormSubcategories(CategoryId, SubcategoryId);
    $("#txtDescription").val(Description);
    $("#ddAccessMode").val(AccessMode);
    $("#txtStartdate").val(StartDate);
    $("#txtEnddate").val(EndDate);
    $("#txtPublishedBy").val(PublishedBy);
    $("#txtDuration").val(Duration);
    $("#txtCourseImage").val(CourseImage);
}

function loadEditFormCategories(id) {
    var ajaxPromise = GetAjaxPromiseCategories();
    clearCategory();
    ajaxPromise.done(function (data) {
        populateSelectCategory(data);
        let CategoryId = $("#ddCategory").val(id);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 0)
            return alert("Could not load Categories Error : connectting to server.");

        var responseMsg = jqXHR.responseJSON;
        console.log(responseMsg);
        console.log(errorThrown);
    });
}

function loadEditFormSubcategories(id, selectedId) {
    var ajaxPromise = GetAjaxPromiseSubcategories(id);
    clearSubcategory();
    ajaxPromise.done(function (data) {
        populateSelectSubcategory(data);
        let SubcategoryId = $("#ddSubcategory").val(selectedId);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 0)
            return alert("Could not load Categories Error : connectting to server.");

        var responseMsg = jqXHR.responseJSON;
        console.log(responseMsg);
        console.log(errorThrown);
    });
}

function postUpdateCourse() {

    let Id = $("#txtId").val().trim();
    let Title = $("#txtTitle").val().trim();
    let CategoryId = $("#ddSubcategory").val();
    let Description = $("#txtDescription").val().trim();
    let AccessMode = $("#ddAccessMode").val().trim();
    let StartDate = $("#txtStartdate").val().trim();
    let EndDate = $("#txtEnddate").val().trim();
    let PublishedBy = $("#txtPublishedBy").val().trim();
    let UpdatedBy = "10146063";
    let Duration = $("#txtDuration").val().trim();
    let CourseImage = $("#txtCourseImage").val().trim();
    let CourseTypeId = $("#ddCourseType").val().trim();

    //TEsting
    //Title = "Course test 1";
    //CategoryId = 1;
    //Description = "lorem";
    //AccessMode = 1;
    //StartDate = "";
    //EndDate = "";
    //PublishedBy = "";
    //UpdatedBy = "";
    //Duration = "";
    //CourseImage = "";
    //CourseTypeId = "";

    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Courses/Update",
        crossDomain: true,
        data: JSON.stringify({
            "Id": Id,
            "Title": Title,
            "CategoryId": CategoryId,
            "Description": Description,
            "AccessMode": AccessMode,
            "StartDate": StartDate,
            "EndDate": EndDate,
            "PublishedBy": PublishedBy,
            "UpdatedBy": UpdatedBy,
            "Duration": Duration,
            "CourseImage": CourseImage,
            "CourseTypeId": CourseTypeId
        }),
        success: function (data, textStatus, jqXHR) {
            ReloadCourseTable();
            ToggleEditCourseModal();
            swal({
                title: "Success",
                text: data.title + " was updated.",
                icon: "success",
                button: "Close",
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                $("#validationSummary").empty();
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.statusText,
                    icon: "error",
                    button: "Ok",
                });

            }
        }
    });
}

function PostDeleteCourse(_id) {
    $.ajax({
        contentType: 'application/json',
        type: "DELETE",
        url: apiEndpoint + "/Courses/Delete/" + _id,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            ReloadCourseTable();
            swal(`Course deleted successfully!`, {
                icon: "success",
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").html('');
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.responseText,
                    icon: "error",
                    button: "Ok",
                });
            }
        }
    });
}

function ToggleEditCourseModal() {
    $("#_editCourseModal").modal("toggle");
}

function initializeImageUploader() {
    $("#frmCourseImageUploader").submit(function (e) {
        e.preventDefault();
        var file = $("input[name=file]").val();

        $.ajax({
            contentType: false,
            cache: false,
            processData: false,
            type: "POST",
            url: apiEndpoint + "/FileUploader/CourseImage",
            crossDomain: true,
            data: new FormData(this),
            success: function (data, textStatus, jqXHR) {

                swal({
                    title: "Success",
                    text: jqXHR.statusText,
                    icon: "success",
                    button: "Close",
                });
                //alert(jqXHR.statusText + " " + categoryName);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                //if (jqXHR.status == "500")
                //    return alert(jqXHR.statusText);
                if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                    var responseMsg = jqXHR.responseJSON;
                    $("#validationSummary").html('');
                    for (var error in responseMsg) {
                        var errorValue = responseMsg[error];
                        $("#validationSummary").append("<li>" + errorValue + "</li>")
                    }
                } else {
                    swal({
                        title: "Error Encountered!",
                        text: jqXHR.responseText,
                        icon: "error",
                        button: "Ok",
                    });
                }
            }
        }).done(function (data) {
            $("#courseImage").attr("src", data.fileLocation);
            $("#txtCourseImage").val(data.fileLocation)
            //lazyLoadImages();
        });

       
    });
}