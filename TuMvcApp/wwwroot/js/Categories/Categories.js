﻿var categoriesList;
var categoriesTable;
var categoriesDataTable;

function LoadCategoriesIntoTable(tableName) {
    categoriesTable = tableName;
    apiGetCategories = apiEndpoint + "/Categories";
    
    $('#' + categoriesTable).DataTable({
        //"processing": true,
        "ajax": {
            "url": apiGetCategories,
            "method": "GET",
            "dataSrc": function (data) {
                categoriesList = data;
                return data;
            },
        } ,
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "parentId" },
            {
                "data": "id", "render": function (data, type, row, meta) {
                    return `<a href="#" class="editor_edit" data-id=${data}>Edit</a> / <a href="#" class="editor_remove" data-id=${data}>Delete</a>`
                }
            }
        ],
        "language": {
            "sSearch": "Filter results:",
            //'processing': 'Loading...'

        }
    });

    // Edit record
    $('#' + categoriesTable).on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        clearCategory();
        populateSelectCategory(categoriesList);

        let id = $(this).attr("data-id");
        $("#_editCategoriesModal").modal("toggle");
        var editCategory = categoriesList.find(c => c.id == id);

        const editCategoryId = editCategory.id;
        const name = editCategory.name;
        const parentId = editCategory.parentId;

        $("input[name=id]").val(editCategoryId);
        $("input[name=name]").val(name);

        if (parentId === null)
            $("#ddCategory").val(-1);
        else
            $("#ddCategory").val(parentId);
    });

    // Delete a record
    $('#' + categoriesTable).on('click', 'a.editor_remove', function (e) {
        e.preventDefault();

        let id = $(this).attr("data-id");
        var deleteCategory = categoriesList.find(c => c.id == id);

        const deleteCategoryId = deleteCategory.id;
        const name = deleteCategory.name;
        const parentId = deleteCategory.parentId;

        swal({
            title: "Confirm",
            text: `Are you sure you want to delete ${name}?` ,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostDeleteCategory(id);
                }
            });

    });

}

function ReloadCategoriesTable() {
    $("#tbl_index_all_categories").DataTable().ajax.reload(function (data) {
        categoriesList = data;
    });
}

function GetAjaxPromiseCategories() {
    var apiGetCategories = apiEndpoint + "/Categories";
    return $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories,
        crossDomain: true
    });
}

function GetAjaxPromiseSubcategories(parentId) {
    var apiGetCategories = apiEndpoint + "/Categories?parentId=" + parentId;
    return $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories,
        crossDomain: true
    });
}

function CreateCategory() {
    //set form values
    let _name, _parentid;
    _name = $('#Name').val().trim();
    _parentid = parseInt($('#ddCategory').val());

    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Categories/Create",
        crossDomain: true,
        data: JSON.stringify({
            Name: _name,
            ParentId: _parentid >= 0 ? _parentid : null
        }),
        success: function (data, textStatus, jqXHR) {
            var categoryName = data.name;
            console.log(data);
            console.log(jqXHR);
            swal({
                title: "Success",
                text: jqXHR.statusText,
                icon: "success",
                button: "Close",
            }).then((response) => {
                window.location = "/Categories";
            });
            //alert(jqXHR.statusText + " " + categoryName);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                $("#validationSummary").html('');
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.responseText,
                    icon: "error",
                    button: "Ok",
                });
            }
        }
    });
}

function clearSubcategory() {
    $('#ddSubcategory').html('');
}

function clearCategory() {
    $('#ddCategory').html('');
}

function enableSelectSubcategory() {
    $("#ddSubcategory").disabled = false;
}

function disableSelectSubcategory() {
    $("#ddSubcategory").disabled = false;
}

function loadSubcategory(parentCategoryId) {
    apiGetCategories = apiEndpoint + "/Categories?parentId=";
    $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories + parentCategoryId,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            populateSelectSubcategory(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 0)
                return alert("Could not connect to server.");

            var responseMsg = jqXHR.responseJSON;
            console.log(responseMsg);
            console.log(errorThrown);
        }
    });
}

function loadCategory() {
    apiGetCategories = apiEndpoint + "/Categories";
    $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            populateSelectCategory(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 0)
                return alert("Could not connect to server.");

            var responseMsg = jqXHR.responseJSON;
            console.log(responseMsg);
            console.log(errorThrown);
        }
    });
}

function populateSelectCategory(categories) {
    //add default select
    let select = $("#ddCategory");

    var option = document.createElement("option");
    option.text = "Select";
    option.value = -1;
    select.append(option);

    categories.map(function (c) {
        var option = document.createElement("option");
        option.text = c.name;
        option.value = c.id;
        select.append(option);
    });


    $('#ddCategory').on('change', function () {

        var selectedCategoryId = this.value;
        console.log("selectedCategoryId = " + selectedCategoryId);
        if (selectedCategoryId > 0) {
            enableSelectSubcategory();
            clearSubcategory();
            loadSubcategory(selectedCategoryId);
        } else {
            clearSubcategory();
            addDefaultSelect("ddSubcategory", "Select Category First");
            disableSelectSubcategory();

        }

    });
}

function populateSelectSubcategory(subcategories) {
    let select = $("#ddSubcategory");
    let defaultSelectItem = "Select";
    if (subcategories.length == 0)
        defaultSelectItem = "No Subcategory Available";

    var option = document.createElement("option");
    option.text = defaultSelectItem;
    option.value = -1;
    select.append(option);

    subcategories.map(function (c) {
        var option = document.createElement("option");
        option.text = c.name;
        option.value = c.id;
        select.append(option);
    });
}

function addDefaultSelect(selectId, defaultItem) {
    let select = $("#" + selectId);
    var option = document.createElement("option");
    option.text = defaultItem;
    option.value = -1;
    select.append(option);
}

function PostEditCategory(_id, _name, _parentid) {
    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Categories/Update",
        crossDomain: true,
        data: JSON.stringify({
            Id: _id,
            Name: _name,
            ParentId: _parentid >= 0 ? _parentid : null
        }),
        success: function (data, textStatus, jqXHR) {
            ReloadCategoriesTable();
            ToggleEditCategoryModal();
            swal({
                title: "Success",
                text: data.name + " was updated.",
                icon: "success",
                button: "Close",
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").html('');
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.responseText,
                    icon: "error",
                    button: "Ok",
                });
            }
        }
    });
}

function PostDeleteCategory(_id) {
    $.ajax({
        contentType: 'application/json',
        type: "DELETE",
        url: apiEndpoint + "/Categories/Delete/" + _id ,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            ReloadCategoriesTable();           
            swal(`Category deleted successfully!`, {
                icon: "success",
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").html('');
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.responseText,
                    icon: "error",
                    button: "Ok",
                });
            }
        }
    });
}

function ToggleEditCategoryModal() {
    $("#_editCategoriesModal").modal("toggle");
}

