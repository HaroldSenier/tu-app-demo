﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApp.Models
{
	public class CourseModel
	{
		public int? Id { get; set; }

		public string Title { get; set; }

		public int CategoryId { get; set; }

		public CategoryModel Category { get; set; }

		public string Description { get; set; }

		public int AccessMode { get; set; }

		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }

		public int PublishedBy { get; set; }

		public DateTime DatePublished { get; set; }

		public DateTime DateModified { get; set; }

		public int Duration { get; set; }

		public string CourseImage { get; set; }

		public int CourseTypeId { get; set; }

	}
}
